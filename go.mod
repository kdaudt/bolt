module github.com/boltdb/bolt

go 1.16

require golang.org/x/sys v0.0.0-20210629170331-7dc0b73dc9fb

retract v1.3.4 // Broken
